const { body } = require('express-validator');
const Role = require('../../utils/userRoles.utils');


exports.createUserSchema = [
    body('Username')
        .exists()
        .withMessage('username is required')
        .isLength({ min: 3 })
        .withMessage('Must be at least 3 chars long'),
    body('Password')
        .exists()
        .withMessage('Password is required')
        .notEmpty()
        .isLength({ min: 6 })
        .withMessage('Password must contain at least 6 characters'),
    body('ConfirmPassword')
        .exists()
        .custom((value, { req }) => value === req.body.Password)
        .withMessage('ConfirmPassword field must have the same value as the password field'),
    body('ProfileImage')
        .optional()
        .isString(),
    body('Fullname')
        .exists()
        .withMessage('Your first name is required')
        .isLength({ min: 3 })
        .withMessage('Must be at least 3 chars long'),
    body('DOB')
        .exists()
        .withMessage('Your date of birth is required')
        .isDate()
        .withMessage('Must be valid date'),
    body('RoleID')
        .exists()
        .withMessage('Role ID is required'),
    body('FacultyID')
        .exists()
        .withMessage('Role ID is required'),
    body('Address')
        .optional()
        .isString(),
    body('PhoneNumber')
        .optional()
        .isMobilePhone(),
    body('Email')
        .optional()
        .isEmail()
        .withMessage('Must be a valid email')
        .normalizeEmail(),
    body('AcademicYearID')
        .exists()
        .withMessage('Academic Year ID is required')
];

exports.updateUserSchema = [
    body('Username')
        .optional()
        .isLength({ min: 3 })
        .withMessage('Must be at least 3 chars long'),
    body('Password')
        .optional()
        .notEmpty()
        .isLength({ min: 6 })
        .withMessage('Password must contain at least 6 characters')
        .custom((value, { req }) => !!req.body.ConfirmPassword)
        .withMessage('Please confirm your password'),
    body('ConfirmPassword')
        .optional()
        .custom((value, { req }) => value === req.body.Password)
        .withMessage('ConfirmPassword field must have the same value as the password field'),
    body('ProfileImage')
        .optional()
        .isString(),
    body('Fullname')
        .optional()
        .isLength({ min: 3 })
        .withMessage('Must be at least 3 chars long'),
    body('DOB')
        .optional(),
    body('RoleID')
        .optional(),
    body('FacultyID')
        .optional(),
    body('Address')
        .optional()
        .isString(),
    body('PhoneNumber')
        .optional()
        .isMobilePhone(),
    body('Email')
        .optional()
        .isEmail()
        .withMessage('Must be a valid email')
        .normalizeEmail(),
    body('AcademicYearID')
        .optional(),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
];

exports.validateLogin = [
    body('Username')
        .exists()
        .withMessage('Username is required')
        .notEmpty()
        .withMessage('Username must be filled'),
    body('Password')
        .exists()
        .withMessage('Password is required')
        .notEmpty()
        .withMessage('Password must be filled')
];