const { body } = require('express-validator');


exports.createFacultySchema = [
    body('FacultyName')
        .exists()
        .withMessage('Faculty name is required'),
    body('Description')
        .optional()
];

exports.updateFacultySchema = [
    body('FacultyName')
        .optional(),
    body('Description')
        .optional(),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
];