const { body } = require('express-validator');

exports.createAcademicSchema = [
    body('AcademicName')
        .exists()
        .withMessage('Academic Name is required'),
    body('AcademicStartDate')
        .exists()
        .withMessage('Start Date is required')
        .isDate()
        .withMessage('Start Date must be valid date'),
    body('AcademicEndDate')
        .exists()
        .withMessage('End Date is required')
        .isDate()
        .withMessage('End Date must be valid date')
];

exports.updateAcademicSchema = [
    body('AcademicName')
        .optional(),
    body('AcademicStartDate')
        .optional()
        .isDate()
        .withMessage('Start Date must be valid date'),
    body('AcademicEndDate')
        .optional()
        .isDate()
        .withMessage('End Date must be valid date'),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
];