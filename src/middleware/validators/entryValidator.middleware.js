const { body } = require('express-validator');

exports.createEntrySchema = [
    body('EntryName')
        .exists()
        .withMessage('Academic Name is required'),
    body('Description')
        .optional(),
    body('StartDate')
        .exists()
        .withMessage('Start Date is required')
        .isDate()
        .withMessage('Start Date must be valid date'),
    body('EndDate')
        .exists()
        .withMessage('End Date is required')
        .isDate()
        .withMessage('End Date must be valid date')
];

exports.updateEntrySchema = [
    body('EntryName')
        .optional(),
    body('Description')
        .optional(),
    body('StartDate')
        .optional()
        .isDate()
        .withMessage('Start Date must be valid date'),
    body('EndDate')
        .optional()
        .isDate()
        .withMessage('End Date must be valid date'),
    body()
        .custom(value => {
            return !!Object.keys(value).length;
        })
        .withMessage('Please provide required field to update')
];