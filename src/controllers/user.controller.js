const fs = require("fs");
const path = require("path");
const UserModel = require('../models/user.model');
const HttpException = require('../utils/HttpException.utils');
const { compress, formatFilter } = require('../utils/common.utils')
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class UserController {
    getAllUsers = async (req, res, next) => {
        const {limit, offset, filter, ...reqQuery} = req.query;

        let filterObject = filter ? formatFilter(filter) : filter;

        let userList = filterObject? await UserModel.findLike({...filterObject, 'orderBy' : reqQuery.orderBy}) : await UserModel.find(reqQuery);

        userList = userList.map(user => {
            const { Password, ...userWithoutPassword } = user;
            return userWithoutPassword;
        });

        res.send(compress(userList, limit, offset));
    };

    getUserById = async (req, res, next) => {
        const user = await UserModel.findOne({ id: req.params.id });
        if (!user) {
            throw new HttpException(404, 'User not found');
        }

        const { Password, ...userWithoutPassword } = user;

        res.send(userWithoutPassword);
    };

    getUserByuserName = async (req, res, next) => {
        const user = await UserModel.findOne({ Username: req.params.username });
        if (!user) {
            throw new HttpException(404, 'User not found');
        }

        const { Password, ...userWithoutPassword } = user;

        res.send(userWithoutPassword);
    };

    getCurrentUser = async (req, res, next) => {
        const { Password, ...userWithoutPassword } = req.currentUser;

        res.send(userWithoutPassword);
    };

    createUser = async (req, res, next) => {
        this.checkValidation(req);

        await this.hashPassword(req);

        const result = await UserModel.create(req.body);

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        const user = await UserModel.findOne({ id: result });

        const { Password, ...userWithoutPassword } = user;

        res.send(userWithoutPassword);
    };

    updateUser = async (req, res, next) => {
        this.checkValidation(req);

        await this.hashPassword(req);

        const { ConfirmPassword, ...restOfUpdates } = req.body;

        // do the update query and get the result
        // it can be partial edit
        const result = await UserModel.update(restOfUpdates, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows } = result;

        if (!affectedRows) {
            throw new HttpException(404, 'User not found');
        }

        const user = await UserModel.findOne({ id: req.params.id });

        const { Password, ...userWithoutPassword } = user;

        res.send(userWithoutPassword);
    };

    deleteUser = async (req, res, next) => {
        const result = await UserModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'User not found');
        }
        res.send({message:'User has been deleted'});
    };

    userLogin = async (req, res, next) => {
        this.checkValidation(req);

        const { Username, Password: pass } = req.body;

        const user = await UserModel.findOne({ Username: Username });
        
        if (!user) {
            throw new HttpException(401, 'Unable to login!');
        }

        const isMatch = await bcrypt.compare(pass, user.Password);

        if (!isMatch) {
            throw new HttpException(401, 'Incorrect password!');
        }

        // user matched!
        const secretKey = process.env.SECRET_JWT || "";
        const token = jwt.sign({ user_id: user.id.toString() }, secretKey, {
            expiresIn: '24h'
        });

        const { Password, ...userWithoutPassword } = user;

        res.send({ ...userWithoutPassword, token });
    };

    checkValidation = (req) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            throw new HttpException(400, 'Validation faild', errors);
        }
    }

    // hash password if it exists
    hashPassword = async (req) => {
        if (req.body.Password) {
            req.body.Password = await bcrypt.hash(req.body.Password, 8);
        }
    }
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new UserController;