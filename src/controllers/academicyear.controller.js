const AcademicModel = require('../models/academicyear.model');
const HttpException = require('../utils/HttpException.utils');
const { compress, formatFilter } = require('../utils/common.utils')
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Academic Year Controller
 ******************************************************************************/
class AcademicController {
    getAllAcademics = async (req, res, next) => {
        const {limit, offset, filter, ...reqQuery} = req.query;

        let filterObject = filter ? formatFilter(filter) : filter;

        let academicList = filterObject ? await AcademicModel.findLike({...filterObject, 'orderBy' : reqQuery.orderBy}) : await AcademicModel.find(reqQuery);

        academicList = academicList.filter(academic => academic.AcademicName !== "Staff");

        res.send(compress(academicList, limit, offset));
    };

    getAcademicById = async (req, res, next) => {
        const academic = await AcademicModel.findOne({ id: req.params.id });
        if (!academic) {
            throw new HttpException(404, 'Academic Year not found');
        }

        res.send(academic);
    };

    createAcademic = async (req, res, next) => {
        const result = await AcademicModel.create(req.body);

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        const academic = await AcademicModel.findOne({ id: result });

        res.send(academic);
    };

    updateAcademic = async (req, res, next) => {
        // do the update query and get the result
        // it can be partial edit
        const result = await AcademicModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows } = result;

        if (!affectedRows) {
            throw new HttpException(404, 'Academic Year not found');
        }

        const academic = await AcademicModel.findOne({ id: req.params.id });

        res.send(academic);
    };

    deleteAcademic = async (req, res, next) => {
        const result = await AcademicModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Academic Year not found');
        }
        res.send({message: 'Academic Year has been deleted'});
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new AcademicController;