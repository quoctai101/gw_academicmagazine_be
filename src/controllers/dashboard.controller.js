const DashboardModel = require('../models/dashboard.model');
const HttpException = require('../utils/HttpException.utils');
const Role = require('../utils/userRoles.utils');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Dashboard Controller
 ******************************************************************************/
class DashboardController {
    getDashboard = async (req, res, next) => {
        var statistics;

        switch (req.currentUser.RoleName) {
            case Role.admin:
                statistics = await DashboardModel.adminQuery();
                break;
            case Role.marketingManager:
                statistics = await DashboardModel.managerQuery();
                break;
            case Role.marketingCoordinator:
                statistics = await DashboardModel.coorQuery();
                break;
            case Role.student:
                statistics = await DashboardModel.studentQuery(req.currentUser.id);
                break;
            default:
                break;
        }
        
        res.send(statistics);
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new DashboardController;