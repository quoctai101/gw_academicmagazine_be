const FacultyModel = require('../models/faculty.model');
const HttpException = require('../utils/HttpException.utils');
const { compress, formatFilter } = require('../utils/common.utils')
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Faculty Controller
 ******************************************************************************/
class FacultyController {
    getAllFaculties = async (req, res, next) => {
        const {limit, offset, filter, ...reqQuery} = req.query;

        let filterObject = filter ? formatFilter(filter) : filter;

        let facultyList = filterObject? await FacultyModel.findLike({...filterObject, 'orderBy' : reqQuery.orderBy}) : await FacultyModel.find(reqQuery);

        facultyList = facultyList.filter(faculty => faculty.FacultyName !== "Staff")

        res.send(compress(facultyList, limit, offset));
    };

    getFacultyById = async (req, res, next) => {
        const faculty = await FacultyModel.findOne({ id: req.params.id });
        if (!faculty) {
            throw new HttpException(404, 'Faculty not found');
        }

        res.send(faculty);
    };

    createFaculty = async (req, res, next) => {
        const result = await FacultyModel.create(req.body);

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        const faculty = await FacultyModel.findOne({ id: result });

        res.send(faculty);
    };

    updateFaculty = async (req, res, next) => {
        // do the update query and get the result
        // it can be partial edit
        const result = await FacultyModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows } = result;

        if (!affectedRows) {
            throw new HttpException(404, ' Faculty not found');
        }

        const faculty = await FacultyModel.findOne({ id: req.params.id });

        res.send(faculty);
    };

    deleteFaculty = async (req, res, next) => {
        const result = await FacultyModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Faculty not found');
        }
        res.send({message:'Faculty has been deleted'});
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new FacultyController;