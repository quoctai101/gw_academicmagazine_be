const fs = require("fs");
const path = require("path");
const ContributionModel = require('../models/contribution.model');
const StatusModel = require('../models/status.model');
const EntryModel = require('../models/entry.model');
const HttpException = require('../utils/HttpException.utils');
const { compress, getCurrentDate, formatFilter } = require('../utils/common.utils')
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Contribution Controller
 ******************************************************************************/
class ContributionController {
    getAllContributions = async (req, res, next) => {
        const {limit, offset, filter, ...reqQuery} = req.query;

        let filterObject = filter ? formatFilter(filter) : filter;

        let contributionList = filterObject ? await ContributionModel.findLike({...filterObject, 'orderBy' : reqQuery.orderBy})
                                            : await ContributionModel.find(reqQuery);

        contributionList = contributionList.map(contribution => {
            const { Password, ...contributionWithoutPassword } = contribution;
            return contributionWithoutPassword;
        });

        res.send(compress(contributionList, limit, offset));
    };

    getPublicContribution = async (req, res, next) => {
        const {limit, offset, filter, ...reqQuery} = req.query;

        let filterObject = filter ? formatFilter(filter) : filter;

        let contributionList = filterObject ? await ContributionModel.findLike({...filterObject, 'StatusName' : 'selected', 'orderBy' : reqQuery.orderBy}) 
                                            : await ContributionModel.find({...reqQuery, 'StatusName' : 'selected'});

        contributionList = contributionList.map(contribution => {
            const { Password, ...contributionWithoutPassword } = contribution;
            return contributionWithoutPassword;
        });

        res.send(compress(contributionList, limit, offset));
    };

    getContributionsByFalculty = async (req, res, next) => {
        let contributionList = await ContributionModel.find({ FacultyID : req.params.id });

        contributionList = contributionList.map(contribution => {
            const { Password, ...contributionWithoutPassword } = contribution;
            return contributionWithoutPassword;
        });

        res.send(compress(contributionList));
    };

    getContributionsByEntry = async (req, res, next) => {
        let contributionList = await ContributionModel.find({ EntryID : req.params.id });

        contributionList = contributionList.map(contribution => {
            const { Password, ...contributionWithoutPassword } = contribution;
            return contributionWithoutPassword;
        });

        res.send(compress(contributionList));
    };

    getContributionsByStudentId = async (req, res, next) => {
        let contributionList = await ContributionModel.find({ UserID : req.params.id });

        contributionList = contributionList.map(contribution => {
            const { Password, ...contributionWithoutPassword } = contribution;
            return contributionWithoutPassword;
        });

        res.send(compress(contributionList));
    };

    getContributionById = async (req, res, next) => {
        const contribution = await ContributionModel.findOne({ id: req.params.id });
        if (!contribution) {
            throw new HttpException(404, 'Contribution not found');
        }

        const { Password, ...contributionWithoutPassword } = contribution;

        res.send(contributionWithoutPassword);
    };

    createContribution = async (req, res, next) => {
        const {EntryID, ...restOfCreate} = req.body;

        const selectedEntry = await EntryModel.findOne({ id: EntryID });

        let currentDate = getCurrentDate();
        let closeDate = new Date(selectedEntry.EndDate);

        if(currentDate > closeDate){
            throw new HttpException(406, 'Not accepted! The entry is closed!');
        }

        const statusID  = await StatusModel.create('review');

        if (!statusID) {
            throw new HttpException(500, 'Something went wrong!');
        }

        const result = await ContributionModel.create({...req.body, StatusID : statusID});

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        const contribution = await ContributionModel.findOne({ id: result });

        const { Password, ...contributionWithoutPassword } = contribution;

        res.send(contributionWithoutPassword);
    };

    updateContribution = async (req, res, next) => {
        // do the update query and get the result
        // it can be partial edit
        const result = await ContributionModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows } = result;

        if (!affectedRows) {
            throw new HttpException(404, 'Contribution not found');
        }

        const contribution = await ContributionModel.findOne({ id: req.params.id });

        const { Password, ...contributionWithoutPassword } = contribution;

        res.send(contributionWithoutPassword);
    };

    deleteContribution = async (req, res, next) => {
        const result = await ContributionModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Contribution not found');
        }
        res.send({message:'Contribution has been deleted'});
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new ContributionController;