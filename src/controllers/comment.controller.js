const CommentModel = require('../models/comment.model');
const HttpException = require('../utils/HttpException.utils');
const { compress } = require('../utils/common.utils')
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Comment Controller
 ******************************************************************************/
class CommentController {
    getAllComments = async (req, res, next) => {
        const {limit, offset, ...reqQuery} = req.query;

        let commentList = await CommentModel.find(reqQuery);
        
        commentList = commentList.map(comment => {
            const { Password, ...commentWithoutPassword } = comment;
            return commentWithoutPassword;
        });

        res.send(compress(commentList, limit, offset));
    };

    getCommentsByContribution = async (req, res, next) => {
        let commentList = await CommentModel.find({ ContributionID : req.params.id });
        
        commentList = commentList.map(comment => {
            const { Password, ...commentWithoutPassword } = comment;
            return commentWithoutPassword;
        });

        res.send(compress(commentList));
    };

    getCommentById = async (req, res, next) => {
        const comment = await CommentModel.findOne({ id: req.params.id });
        if (!comment) {
            throw new HttpException(404, 'Comment not found');
        }

        const { Password, ...commentWithoutPassword } = comment;

        res.send(commentWithoutPassword);
    };

    createComment = async (req, res, next) => {
        const result = await CommentModel.create(req.body);

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        const comment = await CommentModel.findOne({ id: result });

        const { Password, ...commentWithoutPassword } = comment;

        res.send(commentWithoutPassword);
    };

    updateComment = async (req, res, next) => {
        // do the update query and get the result
        // it can be partial edit
        const result = await CommentModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows } = result;

        if (!affectedRows) {
            throw new HttpException(404, 'Comment not found!');
        }

        const comment = await CommentModel.findOne({ id: req.params.id });

        const { Password, ...commentWithoutPassword } = comment;

        res.send(commentWithoutPassword);
    };

    deleteComment = async (req, res, next) => {
        const result = await CommentModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Comment not found');
        }
        res.send({message:'Comment has been deleted'});
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new CommentController;