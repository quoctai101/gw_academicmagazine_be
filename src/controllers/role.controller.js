const RoleModel = require('../models/role.model');
const HttpException = require('../utils/HttpException.utils');
const { compress } = require('../utils/common.utils')
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Role Controller
 ******************************************************************************/
class RoleController {
    getAllRoles = async (req, res, next) => {
        let roleList = await RoleModel.find();

        res.send(compress(roleList));
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new RoleController;