const EntryModel = require('../models/entry.model');
const HttpException = require('../utils/HttpException.utils');
const { compress, formatFilter } = require('../utils/common.utils')
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              Entry Controller
 ******************************************************************************/
class EntryController {
    getAllEntries = async (req, res, next) => {
        const {limit, offset, filter, ...reqQuery} = req.query;

        let filterObject = filter ? formatFilter(filter) : filter;

        let entryList = filterObject? await EntryModel.findLike({...filterObject, 'orderBy' : reqQuery.orderBy}) : await EntryModel.find(reqQuery);

        res.send(compress(entryList, limit, offset));
    };

    getEntryById = async (req, res, next) => {
        const entry = await EntryModel.findOne({ id: req.params.id });
        if (!entry) {
            throw new HttpException(404, 'Entry not found');
        }

        res.send(entry);
    };

    createEntry = async (req, res, next) => {
        const result = await EntryModel.create(req.body);

        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }

        const entry = await EntryModel.findOne({ id: result });

        res.send(entry);
    };

    updateEntry = async (req, res, next) => {
        // do the update query and get the result
        // it can be partial edit
        const result = await EntryModel.update(req.body, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows } = result;

        if (!affectedRows) {
            throw new HttpException(404, 'Entry not found');
        }

        const entry = await EntryModel.findOne({ id: req.params.id });

        res.send(entry);
    };

    deleteEntry = async (req, res, next) => {
        const result = await EntryModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'Academic Year not found');
        }
        res.send({message:'Entry has been deleted'});
    };
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new EntryController;