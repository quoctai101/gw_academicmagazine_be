const express = require("express");
const dotenv = require('dotenv');
const cors = require("cors");
const HttpException = require('./utils/HttpException.utils');
const errorMiddleware = require('./middleware/error.middleware');

const userRouter = require('./routes/user.route');
const facultyRouter = require('./routes/faculty.route');
const roleRouter = require('./routes/role.route');
const academicRouter = require('./routes/academicyear.route');
const entryRouter = require('./routes/entry.route');
const contributionRouter = require('./routes/contribution.route');
const fileRouter = require('./routes/file.route');
const commentRouter = require('./routes/comment.route');
const dashboardRouter = require('./routes/dashboard.route');

// Init express
const app = express();
// Init environment
dotenv.config();
// parse requests of content-type: application/json
// parses incoming requests with JSON payloads
app.use(express.urlencoded({extended: true}));
app.use(express.json());
// enabling cors for all requests by using cors middleware
app.use(cors());
// Enable pre-flight
app.options("*", cors());

const port = Number(process.env.PORT || 3331);

app.use(`/api/v1/users`, userRouter);
app.use(`/api/v1/faculties`, facultyRouter);
app.use(`/api/v1/roles`, roleRouter);
app.use(`/api/v1/academics`, academicRouter);
app.use(`/api/v1/entries`, entryRouter);
app.use(`/api/v1/contributions`, contributionRouter);
app.use(`/files`, fileRouter);
app.use(`/api/v1/comments`, commentRouter);
app.use(`/api/v1/dashboard`, dashboardRouter);

// 404 error
app.all('*', (req, res, next) => {
    const err = new HttpException(404, 'Endpoint Not Found');
    next(err);
});

// Error middleware
app.use(errorMiddleware);

// starting the server
app.listen(port, () =>
    console.log(`🚀 Server running on port ${port}!`));


module.exports = app;