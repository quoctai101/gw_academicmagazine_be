exports.multipleColumnSet = (object, tableName = '') => {
    if (typeof object !== 'object') {
        throw new Error('Invalid input');
    }

    const keys = Object.keys(object);
    const values = Object.values(object);

    if(!tableName)
        columnSet = keys.map(key => `${key} = ?`).join(', ');
    else columnSet = keys.map(key => `${tableName}.${key} = ?`).join(', ');

    return {
        columnSet,
        values
    }
}

exports.multipleConditionSet = (object, tableName = '', operator = '=') => {
    if (typeof object !== 'object') {
        throw new Error('Invalid input');
    }

    const keys = Object.keys(object);
    const values = Object.values(object);

    if(!tableName)
        columnSet = keys.map(key => `${key} ${operator} ?`).join(' AND ');
    else columnSet = keys.map(key => `${tableName}.${key} ${operator} ?`).join(' AND ');

    return {
        columnSet,
        values
    }
}

exports.compress = (results, limit = '', offset = '') => {
    returnResult = parseInt(limit) ? results.slice(parseInt(offset), parseInt(offset) + parseInt(limit)) : results;
    
    return {
        results: returnResult,
        total: results.length
    }
}

exports.getCurrentDate = (timezone = 7) => {
    var date = new Date();

    var utcParse = date.getTime() + (date.getTimezoneOffset() * 60000);

    return new Date(utcParse + (3600000*timezone));
}

exports.formatFilter = (filter) => {
    let filterObject = JSON.parse(filter);
    
    Object.keys(filterObject).map(key => {
        if(filterObject[key]) filterObject[key] = `%${filterObject[key].$like}%`;
    });

    return filterObject;
}

exports.formatOrderBy = (orderBy) => {
    let order = orderBy.charAt(0) == '-' ? 'DESC' : 'ASC';

    orderBy = orderBy.charAt(0) == '-' ? orderBy.substr(1) : orderBy;

    return ` ORDER BY ${orderBy} ${order}`;
}