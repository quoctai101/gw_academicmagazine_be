const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createUserSchema, updateUserSchema, validateLogin } = require('../middleware/validators/userValidator.middleware');

router.get('/', auth(), awaitHandlerFactory(userController.getAllUsers)); // localhost:3000/api/v1/users
router.get('/:id(\\d+)', auth(), awaitHandlerFactory(userController.getUserById)); // localhost:3000/api/v1/users/1
router.get('/username/:username', auth(), awaitHandlerFactory(userController.getUserByuserName)); // localhost:3000/api/v1/users/username/kaysi005
router.get('/me', auth(), awaitHandlerFactory(userController.getCurrentUser)); // localhost:3000/api/v1/users/whoami
router.post('/', auth(Role.admin), createUserSchema, awaitHandlerFactory(userController.createUser)); // localhost:3000/api/v1/users
router.put('/:id', auth(Role.admin), updateUserSchema, awaitHandlerFactory(userController.updateUser)); // localhost:3000/api/v1/users/1
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(userController.deleteUser)); // localhost:3000/api/v1/users/1


router.post('/login', validateLogin, awaitHandlerFactory(userController.userLogin)); // localhost:3000/api/v1/users/login

module.exports = router;