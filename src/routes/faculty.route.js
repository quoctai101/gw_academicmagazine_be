const express = require('express');
const router = express.Router();
const facultyController = require('../controllers/faculty.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createFacultySchema, updateFacultySchema } = require('../middleware/validators/facultyValidator.middleware');


router.get('/', auth(), awaitHandlerFactory(facultyController.getAllFaculties)); // localhost:3000/api/v1/faculties
router.get('/:id', auth(), awaitHandlerFactory(facultyController.getFacultyById)); // localhost:3000/api/v1/faculties/1
router.post('/', auth(Role.admin), createFacultySchema, awaitHandlerFactory(facultyController.createFaculty)); // localhost:3000/api/v1/faculties
router.put('/:id', auth(Role.admin), updateFacultySchema, awaitHandlerFactory(facultyController.updateFaculty)); // localhost:3000/api/v1/faculties/1
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(facultyController.deleteFaculty)); // localhost:3000/api/v1/faculties/1

module.exports = router;