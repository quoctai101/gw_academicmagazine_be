const express = require('express');
const router = express.Router();
const entryController = require('../controllers/entry.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createEntrySchema, updateEntrySchema } = require('../middleware/validators/entryValidator.middleware');


router.get('/', auth(), awaitHandlerFactory(entryController.getAllEntries)); // localhost:3000/api/v1/entries
router.get('/:id', auth(), awaitHandlerFactory(entryController.getEntryById)); // localhost:3000/api/v1/entries/1
router.post('/', auth(Role.admin), createEntrySchema, awaitHandlerFactory(entryController.createEntry)); // localhost:3000/api/v1/entries
router.put('/:id', auth(Role.admin), updateEntrySchema, awaitHandlerFactory(entryController.updateEntry)); // localhost:3000/api/v1/entries/1
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(entryController.deleteEntry)); // localhost:3000/api/v1/entries/1

module.exports = router;