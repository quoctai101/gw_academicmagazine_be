const express = require('express');
const router = express.Router();
const academicController = require('../controllers/academicyear.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { createAcademicSchema, updateAcademicSchema } = require('../middleware/validators/academicValidator.middleware');


router.get('/', auth(), awaitHandlerFactory(academicController.getAllAcademics)); // localhost:3000/api/v1/academics
router.get('/:id', auth(), awaitHandlerFactory(academicController.getAcademicById)); // localhost:3000/api/v1/academics/1
router.post('/', auth(Role.admin), createAcademicSchema, awaitHandlerFactory(academicController.createAcademic)); // localhost:3000/api/v1/academics
router.put('/:id', auth(Role.admin), updateAcademicSchema, awaitHandlerFactory(academicController.updateAcademic)); // localhost:3000/api/v1/academics/1
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(academicController.deleteAcademic)); // localhost:3000/api/v1/academics/1

module.exports = router;