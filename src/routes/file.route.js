const express = require('express');
const router = express.Router();
const fs = require("fs");
const path = require("path");
const multer = require("multer");

const fileUploader = multer({dest: __dirname + '/uploads'});

router.get('/:name', (req, res) => {
    res.sendFile(__dirname + '/uploads/' + req.params.name);
}); // localhost:3000/files/<filename>

router.post('/', fileUploader.any(), (req, res) => {
    const file = req.files[0];
    fs.renameSync(file.path, file.path + path.extname(file.originalname));
    let fileName = file.filename + path.extname(file.originalname);
    res.send({fileName : fileName});
}); // localhost:3000/files
module.exports = router;