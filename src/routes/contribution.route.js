const express = require('express');
const router = express.Router();
const contributionController = require('../controllers/contribution.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');

const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

router.get('/', auth(), awaitHandlerFactory(contributionController.getAllContributions)); // localhost:3000/api/v1/contributions
router.get('/:id(\\d+)', auth(), awaitHandlerFactory(contributionController.getContributionById)); // localhost:3000/api/v1/contributions/1
router.get('/faculty/:id', auth(), awaitHandlerFactory(contributionController.getContributionsByFalculty)); // localhost:3000/api/v1/contributions/faculty/1
router.get('/public', awaitHandlerFactory(contributionController.getPublicContribution)); // localhost:3000/api/v1/contributions/puublic
router.get('/entry/:id', auth(), awaitHandlerFactory(contributionController.getContributionsByEntry)); // localhost:3000/api/v1/contributions/entry/1
router.get('/student/:id', auth(), awaitHandlerFactory(contributionController.getContributionsByStudentId)); // localhost:3000/api/v1/contributions/student/1
router.post('/', auth(Role.student), awaitHandlerFactory(contributionController.createContribution)); // localhost:3000/api/v1/contributions
router.put('/:id', auth(), awaitHandlerFactory(contributionController.updateContribution)); // localhost:3000/api/v1/contributions/1
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(contributionController.deleteContribution)); // localhost:3000/api/v1/contributions/1

module.exports = router;