const express = require('express');
const router = express.Router();
const commentController = require('../controllers/comment.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

router.get('/', auth(), awaitHandlerFactory(commentController.getAllComments)); // localhost:3000/api/v1/comments
router.get('/:id', auth(), awaitHandlerFactory(commentController.getCommentById)); // localhost:3000/api/v1/comments/1
router.get('/contribution/:id', auth(), awaitHandlerFactory(commentController.getCommentsByContribution)); // localhost:3000/api/v1/comments/contribution/1
router.post('/', auth(), awaitHandlerFactory(commentController.createComment)); // localhost:3000/api/v1/comments
router.put('/:id', auth(Role.admin), awaitHandlerFactory(commentController.updateComment)); // localhost:3000/api/v1/comments/1
router.delete('/:id', auth(Role.admin), awaitHandlerFactory(commentController.deleteComment)); // localhost:3000/api/v1/comments/1

module.exports = router;