const express = require('express');
const router = express.Router();
const roleController = require('../controllers/role.controller');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

router.get('/', auth(Role.admin), awaitHandlerFactory(roleController.getAllRoles)); // localhost:3000/api/v1/roles

module.exports = router;