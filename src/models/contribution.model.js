const query = require('../db/db-connection');
const { multipleColumnSet, multipleConditionSet, formatOrderBy } = require('../utils/common.utils');
class ContributionModel {
    tableName = 'contribution';

    find = async (params = {}) => {
        let sql = `SELECT ${this.tableName}.*, EntryName, entry.Description, StartDate, EndDate, StatusName, UpdateTime, Username, ProfileImage, Fullname, DOB, FacultyName, Address, PhoneNumber, Email
        FROM ${this.tableName}
        LEFT JOIN entry ON ${this.tableName}.EntryID = entry.id
        LEFT JOIN user ON ${this.tableName}.UserID = user.id
        LEFT JOIN status ON ${this.tableName}.StatusID = status.id
        LEFT JOIN faculty ON user.FacultyID = faculty.id`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        let { orderBy, ...restOfParams } = params;

        const { columnSet, values } = multipleConditionSet(restOfParams);

        sql += values.length ? ` WHERE ${columnSet}` : '';

        sql += orderBy ? formatOrderBy(orderBy) : '' ;

        return await query(sql, [...values]);
    }

    findLike = async (params = {}) => {
        let sql = `SELECT ${this.tableName}.*, EntryName, entry.Description, StartDate, EndDate, StatusName, UpdateTime, Username, ProfileImage, Fullname, DOB, FacultyName, Address, PhoneNumber, Email
        FROM ${this.tableName}
        LEFT JOIN entry ON ${this.tableName}.EntryID = entry.id
        LEFT JOIN user ON ${this.tableName}.UserID = user.id
        LEFT JOIN status ON ${this.tableName}.StatusID = status.id
        LEFT JOIN faculty ON user.FacultyID = faculty.id`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        let { orderBy, ...restOfParams } = params;

        const { columnSet, values } = multipleConditionSet(restOfParams, '', 'LIKE')
        
        sql += values.length ? ` WHERE ${columnSet}` : '';

        sql += orderBy ? formatOrderBy(orderBy) : '' ;

        return await query(sql, [...values]);
    }

    findOne = async (params) => {
        if(params.id)
            var { columnSet, values } = multipleColumnSet(params, this.tableName)
        else var { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT ${this.tableName}.*, EntryName, entry.Description, StartDate, EndDate, StatusName, UpdateTime, Username, ProfileImage, Fullname, DOB, FacultyName, Address, PhoneNumber, Email
        FROM ${this.tableName}
        LEFT JOIN entry ON ${this.tableName}.EntryID = entry.id
        LEFT JOIN user ON ${this.tableName}.UserID = user.id
        LEFT JOIN status ON ${this.tableName}.StatusID = status.id
        LEFT JOIN faculty ON user.FacultyID = faculty.id
        WHERE ${columnSet}`;

        const result = await query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }

    create = async ({ Name, Desc, Image, File, EntryID, StatusID, UserID}) => {
        const sql = `INSERT INTO ${this.tableName}
        (contributionname, contributiondesc, contributionimage, contributionfile, entryid, statusid, userid) VALUES (?,?,?,?,?,?,?)`;

        const result = await query(sql, [Name, Desc, Image, File, EntryID, StatusID, UserID]);
        const insertId = result ? result.insertId : 0;

        return insertId;
    }

    update = async (params, id) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `UPDATE ${this.tableName}
        LEFT JOIN entry ON ${this.tableName}.EntryID = entry.id
        LEFT JOIN user ON ${this.tableName}.UserID = user.id
        LEFT JOIN status ON ${this.tableName}.StatusID = status.id
        SET ${columnSet} WHERE ${this.tableName}.id = ?`;

        const result = await query(sql, [...values, id]);

        return result;
    }

    delete = async (id) => {
        const sql = `DELETE FROM ${this.tableName}
        WHERE id = ?`;
        const result = await query(sql, [id]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
}

module.exports = new ContributionModel;