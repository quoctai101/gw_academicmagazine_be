const query = require('../db/db-connection');
const { multipleColumnSet, multipleConditionSet } = require('../utils/common.utils');
class CommentModel {
    tableName = 'comment';

    find = async (params = {}) => {
        let sql = `SELECT * FROM ${this.tableName}
        LEFT JOIN user ON ${this.tableName}.UserID = user.id
        LEFT JOIN contribution ON ${this.tableName}.ContributionID = contribution.id
        `;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        const { columnSet, values } = multipleColumnSet(params)
        sql += ` WHERE ${columnSet}`;

        return await query(sql, [...values]);
    }

    findOne = async (params) => {
        if(params.id)
            var { columnSet, values } = multipleColumnSet(params, this.tableName)
        else var { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT * FROM ${this.tableName}
        LEFT JOIN user ON ${this.tableName}.UserID = user.id
        LEFT JOIN contribution ON ${this.tableName}.ContributionID = contribution.id
        WHERE ${columnSet}`;

        const result = await query(sql, [...values]);

        // return back the first row
        return result[0];
    }

    create = async ({ UserID, ContributionID, Content }) => {
        const sql = `INSERT INTO ${this.tableName}
        (userid, contributionid, content) VALUES (?,?,?)`;

        const result = await query(sql, [UserID, ContributionID, Content]);
        const insertId = result ? result.insertId : 0;

        return insertId;
    }

    update = async (params, id) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;

        const result = await query(sql, [...values, id]);

        return result;
    }

    delete = async (id) => {
        const sql = `DELETE FROM ${this.tableName}
        WHERE id = ?`;
        const result = await query(sql, [id]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
}

module.exports = new CommentModel;