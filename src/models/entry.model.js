const query = require('../db/db-connection');
const { multipleColumnSet, multipleConditionSet, formatOrderBy } = require('../utils/common.utils');
class EntryModel {
    tableName = 'entry';

    find = async (params = {}) => {
        let sql = `SELECT * FROM ${this.tableName}`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        let { orderBy, ...restOfParams } = params;

        const { columnSet, values } = multipleConditionSet(restOfParams);

        sql += values.length ? ` WHERE ${columnSet}` : '';

        sql += orderBy ? formatOrderBy(orderBy) : '' ;

        return await query(sql, [...values]);
    }

    findLike = async (params = {}) => {
        let sql = `SELECT * FROM ${this.tableName}`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        let { orderBy, ...restOfParams } = params;

        const { columnSet, values } = multipleConditionSet(restOfParams, '', 'LIKE')
        
        sql += values.length ? ` WHERE ${columnSet}` : '';

        sql += orderBy ? formatOrderBy(orderBy) : '' ;

        return await query(sql, [...values]);
    }

    findOne = async (params) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

        const result = await query(sql, [...values]);

        // return back the first row
        return result[0];
    }

    create = async ({ EntryName, StartDate, EndDate, Description = '' }) => {
        const sql = `INSERT INTO ${this.tableName}
        (entryname, description, startdate, enddate) VALUES (?,?,?,?)`;

        const result = await query(sql, [EntryName, Description, StartDate, EndDate]);
        const insertId = result ? result.insertId : 0;

        return insertId;
    }

    update = async (params, id) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;

        const result = await query(sql, [...values, id]);

        return result;
    }

    delete = async (id) => {
        const sql = `DELETE FROM ${this.tableName}
        WHERE id = ?`;
        const result = await query(sql, [id]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
}

module.exports = new EntryModel;