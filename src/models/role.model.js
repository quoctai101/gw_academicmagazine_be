const query = require('../db/db-connection');
class RoleModel {
    tableName = 'role';

    find = async (params = {}) => {
        let sql = `SELECT * FROM ${this.tableName}`;

        return await query(sql);
    }
}

module.exports = new RoleModel;