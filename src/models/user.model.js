const query = require('../db/db-connection');
const { multipleColumnSet, multipleConditionSet, formatOrderBy } = require('../utils/common.utils');
class UserModel {
    tableName = 'user';

    find = async (params = {}) => {
        let sql = `SELECT ${this.tableName}.*, RoleName, FacultyName, Description, AcademicName, AcademicStartDate, AcademicEndDate FROM ${this.tableName}
        LEFT JOIN role ON ${this.tableName}.RoleID = role.id
        LEFT JOIN faculty ON ${this.tableName}.FacultyID = faculty.id
        LEFT JOIN academicyear ON ${this.tableName}.AcademicYearID = academicyear.id`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        let { orderBy, ...restOfParams } = params;

        const { columnSet, values } = multipleConditionSet(restOfParams);

        sql += values.length ? ` WHERE ${columnSet}` : '';

        sql += orderBy ? formatOrderBy(orderBy) : '' ;

        return await query(sql, [...values]);
    }

    findLike = async (params = {}) => {
        let sql = `SELECT ${this.tableName}.*, RoleName, FacultyName, Description, AcademicName, AcademicStartDate, AcademicEndDate FROM ${this.tableName}
        LEFT JOIN role ON ${this.tableName}.RoleID = role.id
        LEFT JOIN faculty ON ${this.tableName}.FacultyID = faculty.id
        LEFT JOIN academicyear ON ${this.tableName}.AcademicYearID = academicyear.id`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        let { orderBy, ...restOfParams } = params;

        const { columnSet, values } = multipleConditionSet(restOfParams, '', 'LIKE')
        
        sql += values.length ? ` WHERE ${columnSet}` : '';

        sql += orderBy ? formatOrderBy(orderBy) : '' ;

        return await query(sql, [...values]);
    }

    findOne = async (params) => {
        if(params.id)
            var { columnSet, values } = multipleColumnSet(params, this.tableName)
        else var { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT ${this.tableName}.*, RoleName, FacultyName, Description, AcademicName, AcademicStartDate, AcademicEndDate FROM ${this.tableName}
        LEFT JOIN role ON ${this.tableName}.RoleID = role.id
        LEFT JOIN faculty ON ${this.tableName}.FacultyID = faculty.id
        LEFT JOIN academicyear ON ${this.tableName}.AcademicYearID = academicyear.id
        WHERE ${columnSet}`;

        const result = await query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }

    create = async ({ Username, Password, Fullname, DOB, AcademicYearID, RoleID, FacultyID, Address = '', PhoneNumber = '', Email = '', ProfileImage = '' }) => {
        const sql = `INSERT INTO ${this.tableName}
        (username, password, fullname, dob, academicyearid, roleid, facultyid, address, phonenumber, email, profileimage) VALUES (?,?,?,?,?,?,?,?,?,?,?)`;

        const result = await query(sql, [Username, Password, Fullname, DOB, AcademicYearID, RoleID, FacultyID, Address, PhoneNumber, Email, ProfileImage]);
        const insertId = result ? result.insertId : 0;

        return insertId;
    }

    update = async (params, id) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `UPDATE user SET ${columnSet} WHERE id = ?`;

        const result = await query(sql, [...values, id]);

        return result;
    }

    delete = async (id) => {
        const sql = `DELETE FROM ${this.tableName}
        WHERE id = ?`;
        const result = await query(sql, [id]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
}

module.exports = new UserModel;