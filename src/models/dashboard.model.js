const query = require('../db/db-connection');
class DashboardModel {
    adminQuery = async () => {
        let sql = `SELECT (SELECT COUNT(id) FROM user) as 'total_user', 
        (SELECT COUNT(id) FROM faculty) as 'total_faculty',
        (SELECT COUNT(id) FROM entry) as 'total_entry',
        (SELECT COUNT(id) FROM contribution) as 'total_contribution',
        (SELECT COUNT(id) FROM academicyear) as 'total_academic'`;

        const result = await query(sql);

        return result[0];
    }

    managerQuery = async () => {
        let sql = `SELECT
        (SELECT COUNT(contribution.id) FROM contribution LEFT JOIN status ON contribution.StatusID = status.id WHERE StatusName = 'selected') as 'total_selected_contribution',
        (SELECT COUNT(id) FROM contribution) as 'total_contribution',
        (SELECT COUNT(id) FROM entry) as 'total_entry'`;

        const result = await query(sql);

        return result[0];
    }

    coorQuery = async () => {
        let sql = `SELECT
        (SELECT COUNT(contribution.id) FROM contribution LEFT JOIN status ON contribution.StatusID = status.id WHERE StatusName = 'review') as 'total_review_contribution',
        (SELECT COUNT(id) FROM contribution) as 'total_contribution'`;

        const result = await query(sql);

        return result[0];
    }

    studentQuery = async (id) => {
        let sql = `SELECT
        (SELECT COUNT(id) FROM contribution WHERE UserID = ${id}) as 'total_uploaded_contribution',
        (SELECT COUNT(contribution.id) FROM contribution LEFT JOIN status ON contribution.StatusID = status.id WHERE StatusName = 'selected' AND UserID = ${id}) as 'total_selected_contribution',
        (SELECT COUNT(entry.id) FROM entry LEFT JOIN contribution ON entry.id = contribution.EntryID WHERE UserID = ${id}) as 'total_participated_entry',
        (SELECT COUNT(id) FROM entry) as 'total_entry'`;

        const result = await query(sql);

        return result[0];
    }
}

module.exports = new DashboardModel;