const query = require('../db/db-connection');
class StatusModel {
    tableName = 'status';

    create = async ({ StatusName = 'review' }) => {
        const sql = `INSERT INTO ${this.tableName}
        (statusname) VALUES (?)`;

        const result = await query(sql, [StatusName]);
        const insertId = result ? result.insertId : 0;

        return insertId;
    }
}

module.exports = new StatusModel;