<p style="text-align: center;"><img src="http://optimus-dev.herokuapp.com/static/media/fullLogo.04100811.png" width="75" height="75" /></p>

> ### Optimus Greenwich Magazine Backend

<a href="http://optimus-dev.herokuapp.com/" target="_blank">LIVE DEMO</a>

# Getting started

To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- Import `magazine.sql` to and change environment configuration (including MySQL) in `.env` file
- `npm run dev` to start the local server

# Code Overview

## Dependencies

- [bcrypt.js](https://github.com/dcodeIO/bcrypt.js)
- [body-parser](https://github.com/expressjs/body-parser)
- [cors](https://github.com/expressjs/cors)
- [dotenv](https://github.com/motdotla/dotenv)
- [express](https://github.com/expressjs/express)
- [express-validator](https://github.com/express-validator/express-validator)
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
- [multer](https://github.com/expressjs/multer)
- [mysql2](https://github.com/sidorares/node-mysql2)

## Dev Dependencies

- [nodemon](https://github.com/remy/nodemon)

## Authentication

Requests are authenticated using the `Authorization` header with a valid JWT. We define two express middlewares in `src/midddlewares/auth.middleware.js` that can be used to authenticate requests. The `required` middleware configures the `express-jwt` middleware using our application's secret and will return a 401 status code if the request cannot be authenticated.
